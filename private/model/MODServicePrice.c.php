<?php
class MODServicePrice extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'servicePrice';
	protected static $db_fieldNames = null;
	
	// Database Variables 
	protected $dbf_serviceId;
	protected $dbf_name;
	protected $dbf_cost;
	protected $dbf_order;
	
	// Create Object
	public static function create(MODService $service, $name, $cost, $order = null)
	{
		return parent::createObject(
			array
			(
				'serviceId'		=> $service(),
				'name'			=> $name,
				'cost'			=> $cost,
				'order'			=> $order
			)
		);
	}
	
	// Get Service
	public function service()
	{
		return MODService::getById($this->dbf_serviceId);
	}
	
	// Set Service
	public function setService(MODService $service)
	{
		$this->dbf_serviceId = $service();
	}
	
	// Get Name
	public function name()
	{
		return $this->dbf_name;
	}
	
	// Set Name
	public function setName($string)
	{
		$this->dbf_name = $string;
	}
	
	// Get Cost
	public function cost()
	{
		return $this->dbf_cost;
	}
	
	// Set Cost
	public function setCost($cost)
	{
		$this->dbf_cost = $cost;
	}
	
	// Get Order
	public function order()
	{
		return $this->dbf_order;
	}
	
	// Set Order
	public function setOrder($order)
	{
		$this->dbf_order = $order;
	}	
	
	// Get All for a service
	public static function getAllForService(MODService $service)
	{
		$sql  = 'SELECT * ';
		$sql .= 'FROM '.self::$db_tableName.' ';
		$sql .= 'WHERE serviceId = :serviceId ';
		$sql .= 'ORDER BY `order`';
		
		$values = array(':serviceId' => $service());
		
		return self::getBySql($sql,$values);
	}
}
?>