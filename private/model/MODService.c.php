<?php
class MODService extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'service';
	protected static $db_fieldNames = null;
	
	// Database Variables 
	protected $dbf_name;
	protected $dbf_description;
	protected $dbf_order;
	
	// Create Object
	public static function create($name, $description, $order = null)
	{
		return parent::createObject(
			array
			(
				'name'			=> $name,
				'description'	=> $description,
				'order'			=> $order
			)
		);
	}
	
	// Get Name
	public function name()
	{
		return $this->dbf_name;
	}
	
	// Set Name
	public function setName($string)
	{
		$this->dbf_name = $string;
	}
	
	// Get Description
	public function description()
	{
		return $this->dbf_description;
	}
	
	// Set Description
	public function setDescription($string)
	{
		$this->dbf_description = $string;
	}
	
	// Get Order
	public function order()
	{
		return $this->dbf_order;
	}
	
	// Set Order
	public function setOrder($order)
	{
		$this->dbf_order = $order;
	}	
	
	// Get Prices
	public function getPrices()
	{
		return MODServicePrice::getAllForService($this);
	}
}
?>