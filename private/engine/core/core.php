<?php
// Show all errors
error_reporting(-1);
ini_set('display_errors', 1);

// Define Constants
define('PPATH','../private/');
define('GOOGLE_ANALYTICS', 'todo');

// Get Core Functions	
require_once(PPATH.'engine/function/core.f.php');

// Load Request
LoadResource::doLoad(); 

// Display Debug Information
Debug::displayInfo();
?>