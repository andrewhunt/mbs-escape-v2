<?php
class PageGroupContent extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysPageGroupContent';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_pageId;
	protected $dbf_pageGroupId;
	
	// Create Object
	public static function create(Page $page, PageGroup $pageGroup)
	{
		return parent::createObject(
			array
			(
				'pageId'				=> $page(),
				'pageGroupId'			=> $pageGroup()
			)
		);
	}
	
	// Get Page
	public function page()
	{
		return Page::getById($this->dbf_pageId);
	}
	
	// Set Page
	public function setPage(Page $page)
	{
		$this->dbf_pageId = $page();
	}
	
	// Get Page Group
	public function pageGroup()
	{
		return PageGroup::getById($this->dbf_pageGroupId);
	}
	
	// Set Page Group
	public function setPageGroup(PageGroup $pageGroup)
	{
		$this->dbf_pageGroupId = $pageGroup();
	}
	
	// Get all pages that belong to a group
	public static function getAllForPageGroup(PageGroup $pageGroup)
	{
		return Page::getAllForPageGroup($pageGroup);
	}	
	
	// Get all groups that the page belongs to
	public static function getAllForPage(Page $page)
	{
		return PageGroup::getAllForPage($page);
	}
}
?>