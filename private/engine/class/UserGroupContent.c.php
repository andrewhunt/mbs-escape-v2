<?php
class UserGroupContent extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysUserGroupContent';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_userId;
	protected $dbf_userGroupId;
	
	// Create Object
	public static function create(User $user, UserGroup $userGroup)
	{
		return parent::createObject(
			array
			(
				'userId'				=> $user(),
				'userGroupId'			=> $userGroup()
			)
		);
	}
	
	// Get user
	public function user()
	{
		return User::getById($this->dbf_userId);
	}
	
	// Set user
	public function setuser(User $user)
	{
		$this->dbf_userId = $user();
	}
	
	// Get user Group
	public function userGroup()
	{
		return UserGroup::getById($this->dbf_userGroupId);
	}
	
	// Set user Group
	public function setUserGroup(UserGroup $userGroup)
	{
		$this->dbf_userGroupId = $userGroup();
	}
	
	// Get all users that belong to a group
	public static function getAllForUserGroup(UserGroup $userGroup)
	{
		return User::getAllForUserGroup($userGroup);
	}	
	
	// Get all groups that the user belongs to
	public static function getAllForUser(User $user)
	{
		return UserGroup::getAllForUser($user);
	}
}
?>