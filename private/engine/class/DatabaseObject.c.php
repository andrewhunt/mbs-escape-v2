<?php
abstract class DatabaseObject  {
	
	// Class Variables
	protected static $database = null;
	public static $database_selects = 0;
	
	// Instance Variables
	protected $hasChanged = false;
	
	// Common Database Fields
	protected $dbf_id;
	protected $dbf_deleted;
	protected $dbf_created;
	protected $dbf_updated;
	
	
	// Instance Methods
		//
		public function __invoke()
		{
			return $this->id();
		}
		
		
		// Get Id
		public function id()
		{
			return $this->dbf_id;
		}
		
		// Has this object been deleted
		public function deleted()
		{
			return Formatter::booleanDbToPhp($this->dbf_deleted);
		}
		
		// When was this object Created
		public function created()
		{
			return new DateTime($this->dbf_created);
		}
		
		// When was this object last updated
		public function updated()
		{
			return new DateTime($this->dbf_updated);
		}
		
		// Checks if a object has an attribute
		protected function has_attribute($attribute)
		{
			$object_vars = get_object_vars($this);
			return array_key_exists($attribute, $object_vars);
		}
		
		// Data has changed 
		protected function changed()
		{
			$this->hasChanged = true;
		}
		
		public function __destruct() {
			// Check if data was changed
			if ($this->hasChanged)
			{
		    	// Save data back to database
		    	$this->updateDatabase();
			}
		}
	
		// Save all loaded objects
		public static function saveAll()
		{
			// Get all loaded objects
			$loadedobjects = static::$db_loadedObjects;
			foreach ($loadedobjects as $loadedobject) {
				// Check if all fields have changed
				if($loadedobject->hasChanged)
				{
					// Save to database
					$loadedobject->updateDatabase();
				}
			}
		}
	
		/*-----------------*/
		// CRUD Create - Start
		/*-----------------*/
		protected static function createObject($data)
		{
			// Get Database
			$database = self::getDatabase();
			
			// Database Query
            $sql = "INSERT INTO ".static::$db_tableName." (`".join("`, `",
                array_keys($data))."`, `created`) VALUES (:".join(", :",
                array_keys($data)).", NOW())";
                
			// Prepare Query
			$prepare = $database->prepare($sql);
			
			// Create bindings
			$bindingarray = array();
			foreach ($data as $key => $value) {
				$nkey = ':'.$key;
				$bindingarray[$nkey] = $data[$key];
			}
			
			// Execute Query
			$prepare->execute($bindingarray);
			// Get insert id
			$id = $database->lastInsertId();
			// Get calling class
			$className = get_called_class();
			// Create new object
			$newObject = new $className;
			// Set Id
			$newObject->dbf_id = $id;
			// Set other attributes
			foreach ($data as $key => $value) {
				$key = 'dbf_'.$key;
				$newObject->$key = $value;
			}
			
			// Store object in Class
			static::$db_loadedObjects[$id] = $newObject;
			return $newObject;
			
		}
		/*-----------------*/
		// CRUD Create - End
		/*-----------------*/
		
		/*-----------------*/
		// CRUD Read - Start
		/*-----------------*/
		
		
		public static function getBySql($sql,$values = null)
		{
			// Stats update
			self::$database_selects++;
		
			// Save all loaded objects
			static::saveAll();
		
			// Get Database
			$database = self::getDatabase();
			// Prepare Query
			$prepare = $database->prepare($sql);
			// Execute Query
			$prepare->execute($values);
			
			// Set Variable
			$object_array = array();
			// Get Objects
			while ($row = $prepare->fetch(PDO::FETCH_ASSOC)) {
				// Check if object already exists
				if(!array_key_exists($row['id'],static::$db_loadedObjects))
				{
					// Instantiate new object
					static::$db_loadedObjects[$row['id']] = static::instantiate($row);
				}
				$object_array[] = static::$db_loadedObjects[$row['id']];
			}
			// Return object array
			return $object_array;
		}
		
		public static function getOneBySql($sql,$values = null)
		{
			$results = static::getBySql($sql,$values);
			return array_shift($results);
		}
		
		public static function getById($id)
		{
			// Check if object is already loaded
			if(!array_key_exists($id,static::$db_loadedObjects))
				{
					// Get object from the database
					$sql = 'SELECT * FROM '.static::$db_tableName.' ';
					$sql .= 'WHERE id = :id LIMIT 1';
					$values = array(':id'=>$id);
		            $results = static::getBySql($sql,$values);
		            
		            // Check that a object was found
		            if(empty($results))
		            {
		            	throw new Exception('Object doesn\'t exists');
		            }
					return array_shift($results);
				}
			// Return object from class
			return static::$db_loadedObjects[$id];
		}
		
		public static function getAll()
		{
            return static::getBySql("SELECT * FROM `".
                static::$db_tableName."`");
		}
		
		/*-----------------*/
		// CRUD Read - End
		/*-----------------*/
		
		/*-----------------*/
		// CRUD Update - Start
		/*-----------------*/
		protected function updateDatabase()
		{
			// Get Database
			$database = self::getDatabase();
			//
			$fieldNames = static::getFieldNames();
			$fieldSet = array();
			foreach($fieldNames as $value) {
				if($value != 'id')
				{
					$fieldSet[] = "`".$value."` = :".$value;
				}
			}
			
			// Database Query
			$sql = 	"UPDATE `".static::$db_tableName."` SET ";
			$sql .= join(", ", array_values($fieldSet));
			$sql .= " WHERE `id` = :id";
		
			// Prepare Query
			$prepare = $database->prepare($sql);
			// Execute Query
			foreach($fieldNames as $value)
			{
				$valuekey = ':'.$value;
				$valuename = 'dbf_'.$value;
				$bindingarray[$valuekey] = $this->$valuename;
			}
			$prepare->execute($bindingarray);
		}
		/*-----------------*/
		// CRUD Update - End
		/*-----------------*/
		
		/*-----------------*/
		// CRUD Delete - Start
		/*-----------------*/
		public function delete()
		{
			$this->dbf_deleted = '1';
			$this->changed();
		}
		/*-----------------*/
		// CRUD Delete - End
		/*-----------------*/
		
		
		// Create object instance
		protected static function instantiate($data)
		{
			// Find class name
			$className = get_called_class();
			// Create new instance
			$object = new $className;
			// Set properties from database
			foreach ($data as $attribute => $value) {
				$db_attribue = 'dbf_'.$attribute;
				// Check class has attribute
				if($object->has_attribute($db_attribue))
				{
					// Store field in class
					$object->$db_attribue = $value;
				}
			}
			return $object;
		}
		
		// Gets the database instance
		protected static function getDatabase()
		{
			if(is_null(self::$database))
			{
				self::$database = Database::getDatabase();
			}
			return self::$database;
		}
		// Get class fields
		protected static function class_has_attribute($attribute)
		{
			$className = get_called_class();
			$class_vars = get_class_vars($className);
			return array_key_exists($attribute, $class_vars);
		}
		
		// Gets the table fields from the database
		public static function getFieldNames()
		{
			// Check if field names have already been fetched
			if (is_null(static::$db_fieldNames))
			{
				// Get Database
				$database = self::getDatabase();
				// Database Query
				$sql = "DESCRIBE ".static::$db_tableName;
				// Prepare Query
				$prepare = $database->prepare($sql);
				// Execute Query
				$prepare->execute();
				// Bind Columns
				$prepare->bindColumn('Field', $name);
				// Fetch Results
				while($row = $prepare->fetch(PDO::FETCH_BOUND))
				{
					$attribute = 'dbf_'.$name;
					// Check for match
					if(static::class_has_attribute($attribute))
					{
						$fieldNames[] = $name;
					}
				}
				// Store results
				static::$db_fieldNames = $fieldNames;
			}
			// Return results
			return static::$db_fieldNames;
		}
		
		public static function getDbTable()
		{
			return static::$db_tableName;
		}
}
?>
