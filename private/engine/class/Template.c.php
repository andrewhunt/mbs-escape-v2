<?php
class Template extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysTemplate';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_title;
	protected $dbf_description;
	protected $dbf_fileLocation;
	
	// Create Object
	public static function create($title, $description, $fileLocation)
	{
		return parent::createObject(
			array
			(
				'title'			=> $title,
				'description'	=> $description,
				'fileLocation'	=> $fileLocation
			)
		);
	}
	
	// Get Title
	public function title()
	{
		return $this->dbf_title;
	}
	
	// Set Title
	public function setTitle($string)
	{
		$this->dbf_title = $string;
	}
	
	// Get Description
	public function description()
	{
		return $this->dbf_description;
	}
	
	// Set Description
	public function setDescription($string)
	{
		$this->dbf_description = $string;
	}
	
	// Get File Location
	public function fileLocation()
	{
		return $this->dbf_fileLocation;
	}
	
	// Set File Location
	public function setFileLocation($string)
	{
		$this->dbf_fileLocation = $string;
	}
}
?>