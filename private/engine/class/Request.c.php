<?php
class Request {

	private static $requestObject = null;

	private $requestString;
	private $path;
	private $query;
	private $nav = null;
	private $requestPage = null;
	private $loadPage = null;
	
	public static function get()
	{
		if(is_null(self::$requestObject))
		{
			self::$requestObject = new self();
		}
		return self::$requestObject;
	}
	
	
	// Create request Object
	private function __construct()
	{
		// Check for request string
		if(isset($_SERVER['REQUEST_URI']))
		{
			$this->requestString = $_SERVER['REQUEST_URI'];
		}
		else
		{
			$this->requestString = '';
		}
		
		
		// Break request into an array
		$path = explode("/", explode("?",$this->requestString)[0]);
		array_shift($path);
		$this->path = $path;
		
		// Get nav 
		$this->nav = Nav::getByRequest($this);
		
		// Mark nav
		Nav::markBranch($this->nav);
		
		// Get the page
		try
		{
			// Get Page Requested
			$this->requestPage = $this->getNav()->page();
		}
		catch(Exception $e)
		{
			$this->loadPage = Page::getError404();
		}
		
		if(!is_null($this->requestPage))
		{
			// Check permissions
			if(Permission::check($this->requestPage,Login::getUser()))
			{
				$this->loadPage = $this->requestPage;
			}
			else
			{
				if(is_null(Login::getUser()))
				{
					$this->loadPage = Page::getLoginPage();
				}
				else
				{
					$this->loadPage = Page::getError403();
				}
			}
		}
	}
	
	// Get Request String
	public function requestString()
	{
		return $this->requestString;
	}
	
	// Get requested Page
	public function getRequestPage()
	{
		return $this->requestPage;
	}
	
	// Get final Load Page
	public function getLoadPage()
	{
		return $this->loadPage;
	}
	
	
	// Path Valiable
	public function path()
	{
		return $this->path;
	}
	
	// Get the Nav Item
	public function getNav()
	{
		return $this->nav;
	}
	
	// Set query
	public function setQuery($query)
	{
		$this->query = $query;
	}
	
	// Get Query
	public function query()
	{
		return $this->query;
	}
}
?>