<?php
class Domain extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysDomain';
	protected static $db_fieldNames = null;
	
	// Cache
	protected static $currentDomain = null;
	
	// Database Variables
	protected $dbf_domain;
	protected $dbf_domainGroupId;
	
	// Create Object
	public static function create($domain, DomainGroup $domainGroup)
	{
		return parent::createObject(
			array
			(
				'domain'		=> $domain,
				'domainGroupId'	=> $domainGroup()
			)
		);
	}
	
	// Get Domain
	public function domain()
	{
		return $this->dbf_domain;
	}
	
	// Set Domain
	public function setDomain($string)
	{
		$this->dbf_domain = $string;
	}
	
	// Get Domain Group
	public function domainGroup()
	{
		return DomainGroup::getById($this->dbf_domainGroupId);
	}
	
	// Set Domain Group
	public function setDomainGroup(DomainGroup $domainGroup)
	{
		$this->dbf_domainGroupId = $domainGroup();
	}
	
	// Get Current Domain
	public static function getCurrent()
	{
		if(is_null(self::$currentDomain))
		{
			$sql  = 'SELECT * ';
			$sql .= 'FROM '.self::$db_tableName.' ';
			$sql .= 'WHERE domain = :domain ';
			$sql .= 'LIMIT 1';
			
			$values = array('domain' => $_SERVER['HTTP_HOST']);
			
			$result = self::getOneBySql($sql, $values);
			if(empty($result))
			{
				self::$currentDomain = self::getById(0);
			}
			else
			{
				self::$currentDomain = $result;
			}
		}
		return self::$currentDomain;
	}
}
?>