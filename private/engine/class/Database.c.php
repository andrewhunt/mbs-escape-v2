<?php
class Database extends PDO {
	// Database Variables
	
	// User either $DATABASE_HOST & $DATABASE_PORT or $DATABASE_UNIX_SOCKET
	//
	//private static $DATABASE_HOST 			= '127.0.0.1';
	//private static $DATABASE_PORT 			= '';
	private static $DATABASE_UNIX_SOCKET 	= '/tmp/mysql.sock';
	
	private static $DATABASE_USER 			= 'serverroot';
	private static $DATABASE_PASSWORD 		= 'BLUEfuture1';
	private static $DATABASE_NAME 			= 'client_mbsescape2';
	private static $DATABASE_TYPE			= 'mysql';
	
	// Variables
	private static $database				= null;
	
	// Constructor
	public function __construct() {
		
		// Define PDO Options
		$options = array(
			PDO::ATTR_PERSISTENT => true,					// Keep a consistant connection to the database 
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION		// Use exceptions if errors occur 
		);
		
		// Choose unix socket or host
		if(isset(self::$DATABASE_HOST) && !empty(self::$DATABASE_HOST)) {
			// Check if a port was set
			if(isset(self::$DATABASE_PORT) && !empty(self::$DATABASE_PORT)) {
				$databasePort = self::$DATABASE_PORT;
			} else {
				// Default Database Port
				$databasePort = 3306;
			}
			// Build connection statement
			$connectionType = 'host='.self::$DATABASE_HOST.';port='.$databasePort;
		} elseif (isset(self::$DATABASE_UNIX_SOCKET) && !empty(self::$DATABASE_UNIX_SOCKET)) {
			// Build connection statement
			$connectionType = 'unix_socket='.self::$DATABASE_UNIX_SOCKET;
		} else {
			// Throw exception, no database connection string defined
			throw new exception('Neither $DATABASE_HOST or $DATABASE_UNIX_SOCKET are set');
		}
			// Make final connectionion statement
			$connection = self::$DATABASE_TYPE.':'.$connectionType.';dbname='.self::$DATABASE_NAME;
		try {
			parent::__construct($connection,self::$DATABASE_USER,self::$DATABASE_PASSWORD,$options);
		}
		catch (PDOException $e) {
			echo ('Database connection error!\n');
			exit;
		}
	}
	
	// Set Methods
	
	// Get Methods
	public static function getDatabase() {
		
		// Check if database is already set
		if(is_null(self::$database)) {
		
			// Create database object
			self::$database = new Database();
		}
		return self::$database;
	}
	// Methods
	public function prepare($sql,$options = array()) {
		//echo ("<br /><b>Preparing Statement: ". $sql . "</b><br />");
		return parent::prepare($sql,$options);
	}
}
?>