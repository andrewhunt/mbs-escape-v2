<?php
class LoadResource
{
	public static function doLoad()
	{
		/**
		 *  Request Start -----------------------------
		 */
	
		// Get Request
		$request = Request::get();
		
		// Get File Location
		$resourceFileLocation = PPATH.'resources/'.$request->getLoadPage()->fileLocation();
		
		// Get Template Location
		$templateFileLocation = PPATH.'templates/'.$request->getLoadPage()->template()->fileLocation();
		
		/**
		 *  Request End -------------------------------
		 */
		
		/**
		 *  Actions Start -----------------------------
		 */
		
		// Load Core Actions
		include(PPATH.'engine/core/core.a.php');
		
		// Load Resource Actions
		if(file_exists($resourceFileLocation.'.a.php'))
		{
			include($resourceFileLocation.'.a.php');
		}
		
		/**
		 *  Actions End -------------------------------
		 */
		
		/**
		 *  Logic Start -------------------------------
		 */
		
		// Load Core Logic
		include(PPATH.'engine/core/core.l.php');

		// Load Template Logic
		if(file_exists($templateFileLocation.'.l.php'))
		{
			include($templateFileLocation.'.l.php');
		}
		
		// Load Resource Logic
		if(file_exists($resourceFileLocation.'.l.php'))
		{
			include($resourceFileLocation.'.l.php');
		}
		
		/**
		 *  Logic End ---------------------------------
		 */
		
		/**
		 * Page Start ---------------------------------
		 */
		echo '<html>';
		
		/**
		 *  Head Start --------------------------------
		 */
		echo '<head>';
		
		// Load Core Head
		include(PPATH.'engine/core/core.h.php');
		
		// Load Template Head
		if(file_exists($templateFileLocation.'.h.php'))
		{
			include($templateFileLocation.'.h.php');
		}
		
		// Load Resource Head
		if(file_exists($resourceFileLocation.'.h.php'))
		{
			include($resourceFileLocation.'.h.php');
		}
		
		echo '</head>';
		/**
		 *  Head End ----------------------------------
		 */
		
		/**
		 *  Body Start --------------------------------
		 */
		echo '<body>';
		
		// Load Core Body
		include(PPATH.'engine/core/core.b.php');
		
		// Load Template Body Header
		if(file_exists($templateFileLocation.'.bh.php'))
		{
			include($templateFileLocation.'.bh.php');
		}
		
		// Load Resource Body
		if(file_exists($resourceFileLocation.'.b.php'))
		{
			include($resourceFileLocation.'.b.php');
		}
		
		// Load Template Body Footer
		if(file_exists($templateFileLocation.'.bf.php'))
		{
			include($templateFileLocation.'.bf.php');
		}
		
		echo '</body>';
		/**
		 *  Body End -----------------------------------
		 */
		
		echo '</html>'; 
		/**
		 * Page End ---------------------------------
		 */ 
	}
}
?>