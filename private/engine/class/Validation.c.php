<?php
class Validation {
	
	// Class Constants
	const MINUSERNAMELENGTH = 3;
	
	public static function username($string)
	{
		$checks = array(
			'notblank',
			''
		);
	
		self::check($string,$checks,MINUSERNAMELENGTH);
	}
	
	
	
	
	public static function check($string, $checks, $length = null)
	{
		// Blank error array
		$errors = array();
		
		// Do all the checks
		foreach($checks as $check)
		{
			// Goto this check
			switch($check)
			{
				// Check the $string isn't blank
				case "notblank":
					if($string == '')
					{
						$errors[] = 'Cannot be blank';
					}
				break;
			}
			
		}
		
		// Check if any error were generated
		if(empty($errors))
		{
			// Return the errors
			return $errors;
		}
		else
		{
			// No Errors
			return true;
		}
		
		
	}
	
	
}
?>