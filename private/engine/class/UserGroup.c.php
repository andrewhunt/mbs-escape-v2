<?php
class UserGroup extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysUserGroup';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_name;
	
	// Create Object
	public static function create($name)
	{
		return parent::createObject(
			array
			(
				'name'	=> $name
			)
		);
	}
	
	// Get Name
	public function name()
	{
		return $this->dbf_name;
	}
	
	// Set Name
	public function setName($string)
	{
		$this->dbf_name = $string;
	}
	
	public function getAllUsers()
	{
		return UserGroupContent::getAllForUserGroup($this);
	}
	
	public function getPermissions()
	{
		Permission::getAllForUserGroup($this);
	}
	
	public static function getAllForUser(User $user)
	{
		$sql = 'SELECT '.self::$db_tableName.'.* ';
		$sql .= 'FROM '.self::$db_tableName.', sysUserGroupContent ';
		$sql .= 'WHERE '.self::$db_tableName.'.id = sysUserGroupContent.userGroupId ';
		$sql .= 'AND sysUserGroupContent.userId = :userId';
		$values = array(':userId' => $user());
		return self::getBySql($sql,$values);
	}
	
	public function __toString()
	{
		return 'user Group: '.$this->name();
	}
}
?>