<?php
class DomainGroup extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysDomainGroup';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_name;
	protected $dbf_rootNavId;
	protected $dbf_homeNavId;
	
	// Create Object
	public static function create($name, Nav $rootNav, Nav $homeNav)
	{
		return parent::createObject(
			array
			(
				'name'		=> $name,
				'rootNavId'	=> $rootNav(),
				'homeNavId' => $homeNav()
			)
		);
	}
	
	// Get Name
	public function name()
	{
		return $this->dbf_name;
	}
	
	// Set Name
	public function setName($string)
	{
		$this->dbf_name = $string;
	}
	
	// Get Root Nav
	public function rootNav()
	{
		return Nav::getById($this->dbf_rootNavId);
	}
	
	// Set Root Nav
	public function setRootNav(Nav $nav)
	{
		$this->dbf_rootNavId = $nav();
	}
	
	// Get Home Nav
	public function homeNav()
	{
		return Nav::getById($this->dbf_homeNavId);
	}
	
	// Set Home Nav
	public function setHomeNav(Nav $nav)
	{
		$this->dbf_homeNavId = $nav();
	}
}
?>