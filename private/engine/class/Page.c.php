<?php
class Page extends DatabaseObject {

	// Class Constants
	const LOGINPAGE		= 1;
	const ERROR404PAGE  = 1;
	const ERROR403PAGE  = 1;

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysPage';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_windowTitle;
	protected $dbf_templateId;
	protected $dbf_pageTitle;
	protected $dbf_fileLocation;
	
	// Create Object
	public static function create(Template $template, $windowTitle, $pageTitle, $fileLocation)
	{
		return parent::createObject(
			array
			(
				'windowTitle'			=> $windowTitle,
				'pageTitle'				=> $pageTitle,
				'fileLoction'			=> $fileLocation,
				'templateId'			=> $template()
			)
		);
	}
	
	// Get Template
	public function template()
	{
		return Template::getById($this->dbf_templateId);
	}
	
	// Set Template
	public function setTemplate(Template $template)
	{
		$this->dbf_templateId = $template();
	}
	
	// Get Window Title
	public function windowTitle()
	{
		return $this->dbf_windowTitle;
	}
	
	// Set Window Title
	public function setWindowTitle($string)
	{
		$this->dbf_windowTitle = $string;
	}
	
	// Get Page Title
	public function pageTitle()
	{
		return $this->dbf_pageTitle;
	}
	
	// Set Page Title
	public function setPageTitle($string)
	{
		$this->dbf_pageTitle = $string;
	}
	
	// Get File Location
	public function fileLocation()
	{
		return $this->dbf_fileLocation;
	}
	
	// Set File Location
	public function setFileLocation($string)
	{
		$this->dbf_fileLocation = $string;
	}
	
	// Get Page Groups
	public function getPageGroups()
	{
		return PageGroupContent::getAllForPage($this);
	}
	
	// Get URI to Page
	public function getPath()
	{
		return $this->nav()->getPath();
	}
	
	// Show string
	public function __toString()
	{
		return '[[CLASS Page][ID '.$this->id().'][TITLE '.$this->dbf_pageTitle.']]';
	}
	
	// Get all pages for a page group
	public static function getAllForPageGroup(PageGroup $pageGroup)
	{
		// Query
		$sql  = 'SELECT '.self::$db_tableName.'.* ';
		$sql .= 'FROM '.self::$db_tableName.', '.PageGroupContent::getDbTable().' ';
		$sql .= 'WHERE '.self::$db_tableName.'.id = '.PageGroupContent::getDbTable().'.pageId ';
		$sql .= 'AND '.PageGroupContent::getDbTable().'.pageGroupId = :pageGroupId';
		
		// Values
		$values = array(':pageGroupId' => $pageGroup());
		
		// Return
		return self::getBySql($sql,$values);
	}
	
	// Get Page - Login
	public static function getLoginPage()
	{
		try
		{
			return self::getById(self::LOGINPAGE);
		}
		catch(Exception $e)
		{
			throw new Exception('LOGIN PAGE - Not set correctly');
		}
	}
	
	// Get Page - Forbidden
	public static function getError403()
	{
		try
		{
			return self::getById(self::ERROR403PAGE);
		}
		catch(Exception $e)
		{
			throw new Exception('ERROR 403 PAGE - Not set correctly');
		}
	}
	
	// Get Page - Not Found
	public static function getError404()
	{
		try
		{
			return self::getById(self::ERROR404PAGE);
		}
		catch(Exception $e)
		{
			throw new Exception('ERROR 404 PAGE - Not set correctly');
		}
	}
}
?>