<?php
class User extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysUser';
	protected static $db_fieldNames = null;
	
	// Database Variables 
	protected $dbf_username;
	protected $dbf_firstname;
	protected $dbf_lastname;
	protected $dbf_salting;
	protected $dbf_password;
	protected $dbf_email;
	
	// Create Object
	public static function create($username, $firstname, $lastname, $password, $email)
	{
		// Check username
		if($usernameCheck = Validation::username($username))
		{
			
		}
		return parent::createObject(
			array
			(
				
			)
		);
	}
	// Get First Name
	public function firstname()
	{
		return $this->dbf_firstname;
	}
	
	// Set First Name
	public function setFirstname($string)
	{
		$this->dbf_firstname = $string;
	}
	
	// Get Last Name
	public function lastname()
	{
		return $this->dbf_lastname;
	}
	
	// Set Last Name
	public function setLastname($string)
	{
		$this->dbf_lastname = $string;
	}
	
	// Get Full Name
	public function fullname()
	{
		return $this->dbf_firstname . ' ' . $this->dbf_lastname;
	}
	
	// Get Username
	public function username()
	{
		return $this->dbf_username;
	}
	
	// Get user Groups
	public function getUserGroups()
	{
		return UserGroupContent::getAllForUser($this);
	}
	
	// Get all users for a user group
	public static function getAllForUserGroup(UserGroup $userGroup)
	{
		// Query
		$sql  = 'SELECT '.self::$db_tableName.'.* ';
		$sql .= 'FROM '.self::$db_tableName.', '.UserGroupContent::getDbTable().' ';
		$sql .= 'WHERE '.self::$db_tableName.'.id = '.UserGroupContent::getDbTable().'.userId ';
		$sql .= 'AND '.UserGroupContent::getDbTable().'.userGroupId = :userGroupId';
		
		// Values
		$values = array(':userGroupId' => $userGroup());
		
		// Return
		return self::getBySql($sql,$values);
	}
}
?>