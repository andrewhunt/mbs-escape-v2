<?php
class Permission extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysPermission';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_pageGroupId;
	protected $dbf_userGroupId; 
	
	// Create Object
	public static function create(PageGroup $pageGroup, UserGroup $userGroup)
	{
		return parent::createObject(
			array
			(
				'pageGroupId' =>	$pageGroup(),
				'userGroupId' =>	$userGroup()
			)
		);
	}
	
	// Get user group
	public function getUserGroup()
	{
		return UserGroup::getById($this->dbf_userGroupId);
	}
	
	// Set user group
	public function setUserGroup(UserGroup $userGroup)
	{
		$this->dbf_userGroupId = $userGroup();
	}
	
	// Get page group
	public function getPafeGroup()
	{
		return PageGroup::getById($this->dbf_pageGroupId);
	}
	
	// Set user group
	public function setPageGroup(UPageGroup $pageGroup)
	{
		$this->dbf_pageGroupId = $pageGroup();
	}
	
	// Get All for a page group
	public static function getAllForPageGroup(PageGroup $pageGroup)
	{
		$sql  = 'SELECT * ';
		$sql .= 'FROM '.self::$db_tableName.' ';
		$sql .= 'WHERE pageGroupId = :pageGroupId';
		
		$values = array(':pageGroupId' => $pageGroup());
		
		return self::getBySql($sql,$values);
	}
	
	// Get All for a user group
	public static function getAllForUserGroup(UserGroup $userGroup)
	{
		$sql  = 'SELECT * ';
		$sql .= 'FROM '.self::$db_tableName.' ';
		$sql .= 'WHERE userGroupId = :userGroupId';
		
		$values = array(':userGroupId' => $userGroup());
		
		return self::getBySql($sql,$values);
	}
	
	// Get All for an array user groups
	public static function getAllForUserGroupArray($array)
	{
		$userGroupIds = array();
		foreach($array as $userGroup)
		{
			$userGroupIds[] = $userGroup();
		}
		
		$sql  = 'SELECT * ';
		$sql .= 'FROM '.self::$db_tableName.' ';
		$sql .= 'WHERE userGroupId = ';
		$sql .= join(' OR ',$userGroupIds);
		
		return self::getBySql($sql);
	}
	
	// Get All for an array page groups
	public static function getAllForPageGroupArray($array)
	{
		$pageGroupIds = array();
		foreach($array as $pageGroup)
		{
			$pageGroupIds[] = $pageGroup();
		}
		
		$sql  = 'SELECT * ';
		$sql .= 'FROM '.self::$db_tableName.' ';
		$sql .= 'WHERE pageGroupId = :pageGroupId';
		
		$values = array('pageGroupId' => join(' OR ',$pageGroupIds));
		
		return self::getBySql($sql, $values);
	}	
	
	// Check permission
	public static function check(Page $page, User $user = null)
	{
		// Check if the page has permissions
		$pageGroups = $page->getPageGroups();
		if(empty($pageGroups))
		{
			// No permissions are set therefore access is allowed
			return true;
		}
		
		// Check if there is a user
		if(is_null($user))
		{
			// No user therefore no access
			return false;
		}
		
		// Check if the page in just for any logged in user
		$usersPageGroup = PageGroup::getUsersGroup();
		if(in_array($usersPageGroup, $pageGroups))
		{
			// This page just needs any logged in user
			return true;
		}
		
		// Check for specicalized permissions
		$userGroups = $user->getUserGroups();
		if(empty($userGroups))
		{
			// This user doesn't have any specialized permissions
			return false;
		}
		
		$userPermissions = self::getAllForUserGroupArray($userGroups);
		$pagePermissions = self::getAllForPageGroupArray($pageGroups);
		
		foreach($userPermissions as $userPermission)
		{
			foreach($pagePermissions as $pagePermission)
			{
				if($userPermission === $pagePermission)
				{
					// User access found
					return true;
				}
			}
		}
		
		// No permission
		return false;
	}
}
?>