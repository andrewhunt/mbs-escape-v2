<?php
class PageGroup extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysPageGroup';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_name;
	
	// Create Object
	public static function create($name)
	{
		return parent::createObject(
			array
			(
				'name'	=> $name
			)
		);
	}
	
	// Get Name
	public function name()
	{
		return $this->dbf_name;
	}
	
	// Set Name
	public function setName($string)
	{
		$this->dbf_name = $string;
	}
	
	public function getAllPages()
	{
		return PageGroupContent::getAllForPageGroup($this);
	}
	
	public static function getAllForPage(Page $page)
	{
		$sql = 'SELECT '.self::$db_tableName.'.* ';
		$sql .= 'FROM '.self::$db_tableName.', sysPageGroupContent ';
		$sql .= 'WHERE '.self::$db_tableName.'.id = sysPageGroupContent.pageGroupId ';
		$sql .= 'AND sysPageGroupContent.pageId = :pageId';
		$values = array(':pageId' => $page());
		return self::getBySql($sql,$values);
	}
	
	public function getPermissions()
	{
		Permission::getAllForPageGroup($this);
	}
	
	public function __toString()
	{
		return 'Page Group: '.$this->name();
	}
	
	public static function getUsersGroup()
	{
		return self::getById(1);
	}
	
}
?>