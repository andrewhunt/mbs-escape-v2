<?php
class Debug 
{
	public static function displayInfo($displayPhpInfo = false)
	{	
		global $start;
	
		$request = Request::get();

		$array = array();
		if(is_null($user = Login::getUser()))
		{
			$array['User:loggedin'] = 'false';
		}
		else
		{
			$array['User:loggedin'] = 'true';
			$array['User:Id'] = $user();
			$array['User:username'] = $user->username();
			$array['User:name'] = $user->fullname();
		}
		
		
		$array['Nav:Id'] = $request->getNav()->id();
		$array['Nav:Name'] = $request->getNav()->navName();
		if(!is_null($request->getRequestPage()))
		{
			$array['Request:Page:Id'] = $request->getRequestPage()->id();
			$array['Request:Page:Title'] = $request->getRequestPage()->pageTitle();
		}
		else
		{
			$array['Request:Page:Id'] = 'null';
			$array['Request:Page:Title'] = 'null';
		}
		$array['Template:Id'] = $request->getLoadPage()->template()->id();
		$array['Template:Title'] = $request->getLoadPage()->template()->title();
		$array['Load:Page:Id'] = $request->getLoadPage()->id();
		$array['Load:Page:Title'] = $request->getLoadPage()->pageTitle();
		$array['Resource Location'] = PPATH.'resources/'.$request->getLoadPage()->fileLocation();
		$array['Database:SELECT'] = DatabaseObject::$database_selects;
		$array['Request'] = join(', ',$request->path());
		$array['Query'] = join(', ',$request->query());
	
	
		// Calculate Page Load Time
		$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		$finish = $time;
		$total_time = round(($finish - $start), 4);
		$array['PHP:Load Time '] = $total_time.' seconds.';
	
		// Diplay Debug Info
		print "<table>\n";
		foreach($array as $key => $value)
		{
			print "\t<tr>\n";
				print "\t\t<td>\n";
					print "\t\t\t".$key."\n";
				print "\t\t</td>\n";
				print "\t\t<td>\n";
					print "\t\t\t".$value."\n";
				print "\t\t</td>\n";
			print "\t</tr>\n";
		}
		print "</table>\n";
		
		// Display PHP Info
		if($displayPhpInfo)
		{
			phpinfo();
		}
	}
}
?>