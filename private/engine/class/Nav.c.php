<?php
class Nav extends DatabaseObject {

	// Class Variables
	protected static $db_loadedObjects = array();
	
	// Database Table Info
	protected static $db_tableName = 'sysNav';
	protected static $db_fieldNames = null;
	
	// Database Variables
	protected $dbf_parentId;
	protected $dbf_uriName;
	protected $dbf_pageId;
	protected $dbf_navName;
	protected $dbf_hidden;
	protected $dbf_order;
	
	// Object Variables
	protected $inCurrentBranch = false;
	
	// Create Object
	public static function create(Nav $parentNav, Page $page, $uriName, $navName = '', $hidden = false, $order = 0)
	{
		// Check for UriName Conflict
		checkUriNameConflict($parentNav, $uriName);
		
		// Convert hidden
		if($hidden)
		{
			$hidden = 1;
		}
		else
		{
			$hidden = 0;
		}
		
		// Check for unique order
		// TODO
	
		return parent::createObject(
			array(	'parentId'				=> $parentNav->id(),
					'pageId'				=> $page(),
					'uriName'				=> $uriName,
					'navName'				=> $navName,
					'hidden'				=> $hidden,
					'order'					=> $order));
	}

	

	// Mark this branch as current
	public static function markBranch(Nav $nav)
	{
		try
		{
			while(true)
			{
				$nav->markInBranch();
				$nav = $nav->parent();
			}
		}
		catch(Exception $e)
		{
			// Hit root nav
		}
	}

	// Is this in the current branch
	public function inCurrentBranch()
	{
		return $this->inCurrentBranch;
	}

	// Mark this nav as being part of the current branch
	public function markInBranch()
	{
		$this->inCurrentBranch = true;
	}

	// Get Page
	public function page()
	{
		return Page::getById($this->dbf_pageId);	
	}
	
	// Set Page
	public function setPage(Page $page)
	{
		$this->dbf_pageId = $page();
	}

	public function setParent(Nav $newParent)
	{
		self::checkUriNameConflict($newParent, $this->dbf_uriName);
		$this->dbf_parentId = $newParent->id();
	}

	public static function checkUriNameConflict(Nav $parentNav, $uriName)
	{
		// Check if this uriName is unique
		$newParentChildren = $parentNav->getChildren();
		if(!empty($newParentChildren))
		{
			foreach ($newParentChildren as $child)
			{
				if($child->uriName() == $uriName)
				{
					throw new Exception('Nav:checkUriNameConflict - There is already a child with the this uriName');
				}
			}
		}
		
		// Check against home nav object
		if($parentNav === self::getRootNav())
		{
			foreach(self::getHomeNav()->getChildren() as $child)
			{
				if($child->uriName() == $uriName)
				{
					throw new Exception('Nav:checkUriNameConflict - uriName conflict with Home nav child ');
				}
			}
		}
		
		// Check against root nav object
		if($parentNav === self::getHomeNav())
		{
			foreach(self::getRootNav()->getChildren() as $child)
			{
				if($child->uriName() == $uriName)
				{
					throw new Exception('Nav:checkUriNameConflict - uriName conflict with Root nav child ');
				}
			}
		}
		return false;
	}

	public function uriName()
	{
		return $this->dbf_uriName;
	}
	
	public function navName()
	{
		return $this->dbf_navName;
	}

	public function parent()
	{
		// Check if this has a parent
		if($this->dbf_parentId == self::getRootNav()->id())
		{
			throw new Exception('Nav:parent() - Root Nav Exception - you are at the top level of nav');
		}
		
		// Check the parent exists
		try
		{
			$parentNav = self::getById($this->dbf_parentId);
		}
		catch(Exception $e)
		{
			throw $e;
		}
		
		// Return Partent Nav Object
		return $parentNav;
	}

	// Set if this object will be hidden
	public function setHidden($boolean)
	{
		if($boolean)
		{
			$this->dbf_hidden = 1;
		}
		$this->dbf_hidden = 0;
	}

	// Get the hidden status
	public function hidden()
	{
		if($this->dbf_hidden == 1)
		{
			return true;
		}
		return false;
	}
	
	//public function getPage()
	//{
	//	return Page::getForNav($this);
	//}
	
	// Get children of this nav object
	public function getChildren()
	{
		return self::getAllChildrenOfParent($this);
	}
	
	// Get all child nav for a parent
	public static function getAllChildrenOfParent(Nav $parentNav)
	{
		$sql = 'SELECT * FROM '.self::$db_tableName.' ';
		$sql .= 'WHERE `parentId` = :parentId ';
		$sql .= 'AND `id` != :parentId';
		$values = array(':parentId' => $parentNav->id());
		return self::getBySql($sql,$values);
	}
	
	// Get path string
	public function getPath()
	{
		$path = $this->getPathHelper();
		if(empty($path))
		{
			return '/';	
		}
		return $path;
	}
	
	
	// Get path helper
	private function getPathHelper()
	{
		$pathComponent = '';
	
		// Check for a blank path
		if($this->dbf_uriName != '')
		{
			$pathComponent = '/'.$this->dbf_uriName;
		}
		// Check if parent is the root node
		if($this->dbf_parentId == self::getRootNav()->id())
		{
			return $pathComponent;
		}
		return $this->parent()->getPath().$pathComponent;
	}
	
	// Get home nav
	public static function getHomeNav()
	{
		return Domain::getCurrent()->domainGroup()->homeNav();
	}
	
	// Get root nav
	public static function getRootNav()
	{
		return Domain::getCurrent()->domainGroup()->rootNav();
	}
	
	// Get nav from request
	public static function getByRequest(Request $request)
	{
		// Get Request Path
		$path = $request->path();
		$query = array();
		
		// Get Root Nav
		$parentNav = self::getRootNav();
		
		$firstRun = true;
		
		// Find nav for path
		while(true)
		{
			// Get the next uriName from Path
			$uriName = array_shift($path);
			//echo 'CHECK['.$uriName.']';
			// Get Children
			$children = $parentNav->getChildren();
			
			// Check children for uriName match
			foreach($children as $nav)
			{
				// Check for match
				if($nav->uriName() == $uriName)
				{
					$parentNav = $nav;
					$firstRun = false;
					continue 2;
				}
			}
			
			// Move unmatched element back on to the array
			array_unshift($path, $uriName);
			
			// Check for home child
			if($firstRun)
			{
				// Get Home Nav
				$parentNav = self::getHomeNav();
				$firstRun = false;
				continue;
			}
			
			// Check for wildcard nav
			if($uriName != '')
			{
				foreach($children as $nav)
				{
					// Check for match
					if($nav->uriName() == '*')
					{
						$query[] = array_shift($path);
						$parentNav = $nav;
						continue 2;
					}
				}
			}
			
			// Break out of the loop
			break;
		}
		
		// Remove whitespace of query
		if(end($path) == '')
		{
			array_pop($path);
		}
		
		// Set the query path
		$request->setQuery(array_merge($query,$path));
		
		// Return the final nav object
		return $parentNav;
	}
}
?>