<?php
// Auto Class Loader
spl_autoload_register(function ($class_name) {
	if(substr($class_name, 0,3) == 'MOD')
	{
		include PPATH.'model/'.$class_name.'.c.php';
	}
    else
    {
    	include PPATH.'engine/class/'.$class_name.'.c.php';
    }
});
?>