<div class="menu">
	<?php
	foreach($navs as $nav)
	{
		if($nav->inCurrentBranch())
		{
			echo $nav->navName();
		}
		else
		{
			echo '<a href="'.$nav->getPath().'">'.$nav->navName().'</a>';
		}
		echo '&nbsp;&nbsp;&nbsp;';
	}
	?>
</div>