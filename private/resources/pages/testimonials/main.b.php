<i>"Zana has been treating me on a monthly visit and has my body in great 
shape and keeping it that way. Until my visit to Zana I was having so 
much back trouble and would highly recommend her to anyone. Keep up the good 
work you have my business always."</i><br />
<b>Sara</b><br />
<br />
<i>"Zana makes you feel relaxed the moment you walk into the room. As she talks to you and prepares for your massage, you can tell she really has an interest in not only what's going on with your body, but in who you are. You find yourself feeling comfortably assured that you will be receiving a wonderful therapeutic experience."</i><br />
<b>Alley</b><br />
<br />
<i>"I am only a new client to Zana. I was referred to her by my friend who couldn't stop telling me how good she was. Since Zana has been treating me my aches and pain have improved dramatically. She has been fantastic."</i><br />
<b>Mel</b><br />
<br />
<i>"Zana has treated me and I thought professional, courteous and flexible with when she is available."</i><br />
<b>Matt</b><br />
<br />
<i>"I have been seeing Zana over the past 4 months for a massage and I have to say this girl is just magic. The best nights sleep is always after Zana's massage!
She is a highly professional and very caring person in tune with your needs. Whether you need a chat or a sleep, Zana always knows just the sort of massage you need on this particular day! I have always found her to be accommodating especially when I decide I need a massage TODAY (she always manages to fit me in somewhere) and if not, as soon as she possibly can.
With Zana's high regard for her clients and her ability to work her magic, I cannot hesitate to recommend her to anyone suffering from aches and pains, stress and strain or just feeling like they need a little pampering, Zana can help you too!"</i><br />
<b>Amy</b><br />
<br />
<i>"I am an intermediate runner and I run approximately 30-40km a week. During winter, I was in preparation for the half-marathon at Run Melbourne in July followed by the iconic City2Surf run in Sydney in August. I have been going to massage therapists for nearly 10 years now, but never stuck with the same person. Whilst some were good, some were downright bad and some just didn't show enough care or interest in your well-being through their actions or body language. Getting a recommendation for Zana has been an absolute godsend. Due to my constant running, I was experiencing mild lower back discomfort and stressful upper shoulders and also soreness in my glutes and my calves. Since being worked on by Zana, over the course of the last 3-4 months, I have hardly ever experienced any pain or discomfort during my running. Not only does she give you an absolutely amazing massage, she takes the time to talk to you, consult and probe and find out exactly where the sore spots are. Then, rather than just take your word for it, she checks for herself before she begins your massage therapy. Zana has an amazing ability to massage to the core of the issue and ensure that any soreness, stress and pain are taken care of. She finds the problem areas and zeroes in on the painful spots within minutes and whilst the first few minutes are painful as she gently teases the soreness out of your muscles, I have without fail, gone home feeling 100% better than when I lay down on the massage table. Zana has an amazing amount of patience with her clients. She's constantly checking to make sure that you are getting the desired benefits of her massage therapy and no matter if you are over time, she will make the extra effort to ensure that you don't feel rushed and give you her absolute best effort every time. I am truly grateful for her recommendation and will recommend her to anyone whole-heartedly and without hesitation. With her passion for massage therapy, her bright and cheery attitude, her magical fingers and her care for her clients well-being, Zana truly offers you a rich and rewarding massage that will do wonders for your ailing body."</i><br />
<b>Rahul Prasad</b><br />
<br />
<i>"I have been going to see Zana for a deep tissue massage for over 6 months. I run 2 - 3 times a week, do intense personal training sessions a couple times a week as well as spin classes. My body and muscles always feel completely rejuvenated after a session with Zana. After having had massages in the past with other masseuses, I am so happy I came across Zana and would never consider seeing anyone else.  Zana's massages are not only extremely therapeutic to my sore and tired muscles, but the atmosphere and ambiance of her massage room allows me to completely escape into a world of absolute relaxation.<br />
Zana is extremely passionate and professional in her massage services and truly remarkable with every muscle her hands touch."</i><br />
<b>Theresa</b><br />
<br />
<i>"I received massages from Zana during my second and third trimesters of pregnancy and she provided a very professional and personalized massage service.  She has a specific flair and talent for massage and takes great pride in the service she delivers.  She took care to arrange cushions to support me in my advance stage of pregnancy and help me maintain a comfortable position throughout.  Her massage room is clean and beautifully presented in the style of a boutique day spa.  Zana is a very genuine and conscientious person."</i><br />
<b>Susie Webster</b><br />
<br />
<i>"As a full-time dancer I suffered from soreness and muscle tension throughout most of my body, which was severly limiting my dancing. After my first session with Zana I felt a dramatic improvement, and have since had months of regular sessions which have improved and maintained the condition of my body so I can perform at my best. I feel completely safe in Zana's capable hands and I know that I am taking the best care of my body. Zana inspires unflinching loyalty from her clients through her magical results, as well as her genuine interest in their health and well-being. Her massage sessions are not only beneficial, but thoroughly enjoyable."</i><br />
<b>Leonie</b><br />
<br />
<br />
If you would like to share your experiences or testimony please email us at <a href="mailto:mbsescape@gmail.com"><u>mbsescape@gmail.com</u></a>