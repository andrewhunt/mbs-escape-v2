<table cellpadding="4" cellspacing="4">
	<tr>
		<td valign="top">
			<strong>Make a Booking</strong><br />
			If you would like to book an appointment or request information regarding our services. Please complete the form and we will contact you very shortly.
			<br /><br />
			Alternatively you can contact us by:<br />
			<strong>Phone</strong><br />
			0400 346 939<br />
			<strong>Email</strong><br />
			<a href="mailto:mbsescape@gmail.com"><u>mbsescape@gmail.com</u></a><br />
			<br />
			<strong>Trading Hours</strong><br />
			<table>
				<tr>
					<td>
						Mon - Thu&nbsp;&nbsp;&nbsp;
					</td>
					<td align="right">
						10 am to 8 pm
					</td>
				</tr>
				<tr>
					<td>
						Fri - Sat
					</td>
					<td align="right">
						10 am to 4 pm
					</td>
				</tr>
			</table>
			<br />
			<strong>Location</strong><br />
			Bell Street<br />
			Richmond , VIC 3121<br /><br />
			<iframe width="330" height="330" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Bell+St,+Richmond+VIC,+Australia&amp;aq=2&amp;oq=bell+st,+richmond&amp;sll=37.0625,-95.677068&amp;sspn=42.224734,79.013672&amp;ie=UTF8&amp;split=0&amp;hq=&amp;hnear=Bell+St,+Richmond+Victoria+3121,+Australia&amp;t=m&amp;ll=-37.82504,145.001936&amp;spn=0.022373,0.028238&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Bell+St,+Richmond+VIC,+Australia&amp;aq=2&amp;oq=bell+st,+richmond&amp;sll=37.0625,-95.677068&amp;sspn=42.224734,79.013672&amp;ie=UTF8&amp;split=0&amp;hq=&amp;hnear=Bell+St,+Richmond+Victoria+3121,+Australia&amp;t=m&amp;ll=-37.82504,145.001936&amp;spn=0.022373,0.028238&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
			<br />
						<br />We look forward to assisting you!
		</td>
		<td>
			<div class="form">
				<form method="POST" action="mailto:mbsescape@gmail.com" enctype="text/plain">
					<strong>
						Booking Form
					</strong>
					<table>
						<tr>
							<td>Title</td>
							<td><select name="title">
								<option>Mr</option>
								<option>Mrs</option>
								<option>Miss</option>
								<option>Ms</option>
								<option>Dr</option>
							</select></td>
						</tr>
						<tr>
							<td align="">First Name</td>
							<td><input type="text" name="name" value="" /></td>
						</tr>
						<tr>
							<td>Last Name</td>
							<td><input type="text" name="surname" value="" /></td>
						</tr>
						<tr>
							<td>Address</td>
							<td><input type="text" name="address" value="" /></td>
						</tr>
						<tr>
							<td>City/Suburb</td>
							<td><input type="text" name="city" value="" /></td>
						</tr>
						<tr>
							<td>State</td>
							<td><select name="state">
								<option value="Australian Capital Territory">Australian Capital Territory</option>
								  <option value="New South Wales">New South Wales</option>
								  <option value="Northern Territory">Northern Territory</option>
								  <option value="Queensland">Queensland</option>
								  <option value="South Australia">South Australia</option>
								  <option value="Tasmania">Tasmania</option>
								  <option value="Victoria" selected="selected">Victoria</option>
								  <option value="Western Australia">Western Australia  </option>
							</select></td>
						</tr>
						<tr>
							<td>Postcode</td>
							<td><input type="text" name="postcode" value="" /></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><input type="text" name="email" value="" /></td>
						</tr>
						<tr>
							<td>Telephone</td>
							<td><input type="text" name="phone" value="" /></td>
						</tr>
						<tr>
							<td valign="top">Which service/s are
							you interested in?</td>
							<td>
								<input type="checkbox" name="RelaxationSwedishMassage" value="" />Relaxation Swedish Massage<br />
								<input type="checkbox" name="DeepTissueTherapy" value="" />Deep Tissue Therapy<br />
								<input type="checkbox" name="SportsTherapy" value="" />Sports Therapy<br />
								<input type="checkbox" name="PregnancyMassage" value="" />Pregnancy Massage<br />
								<input type="checkbox" name="CustomMassage" value="" />Custom and Trigger Point Therapy<br />
								<input type="checkbox" name="UnwindNeckHeadandShoulderMassage" value="" />Unwind Neck, Head and Shoulder Massage<br />
								<input type="checkbox" name="ExpressBackMassage" value="" />Express Back Massage<br />
								<input type="checkbox" name="HotStonesMassage" value="" />Hot Stones Therapy<br />
								<input type="checkbox" name="CuppingTherapy" value="" />Cupping Therapy<br />
								<input type="checkbox" name="FacialTreatment" value="" />Facial Treatment<br />
								<input type="checkbox" name="FormalMakeup" value="" />Formal Make up<br />
								<input type="checkbox" name="Bridaltrial" value="" />Bridal trial<br />
								<input type="checkbox" name="WeddingDay" value="" />Wedding Day
							</td>
						</tr>
						<tr>
							<td valign="top">Duration</td>
							<td>
								<input type="radio" name="duration" value="30" checked="checked"/>30 Minutes<br />
								<input type="radio" name="duration" value="60" />60 Minutes<br />
								<input type="radio" name="duration" value="90" />90 Minutes
							</td>
						</tr>
						<tr>
							<td>Date</td>
							<td><input type="text" name="date" value="" /></td>
						</tr>
						<tr>
							<td>Time</td>
							<td><input type="text" name="time" value="" /></td>
						</tr>
						<tr>
							<td valign="top">How did you hear about us</td>
							<td><input type="text" name="hearaboutus" value="" /></td>
						</tr>
						<tr>
							<td valign="top">Future Information</td>
							<td><textarea name="futureinformation"></textarea></td>
						</tr>
						<tr>
							<td>URL</td>
							<td><input type="text" name="url" value="" /></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="submit" value="Submit" /><input type="reset" name="reset" value="Reset" /></td>
						</tr>
					</table>
				</form>
			</div>
			
		</td>
	</tr>
</table>
