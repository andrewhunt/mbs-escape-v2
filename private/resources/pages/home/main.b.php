<div class="home">
	<div class="facebook"><a href="http://www.facebook.com/MBSEscape"><img src="/images/facebook.png" alt="facebook" /></a></div>
<div class="home_text">
Thanks for visiting!<br /><br />
We invite you to relax and rejuvenate from your daily life. This is an escape to revitalise your mind, body and soul.<br /><br />
We hope you can find everything you need. <br /><br />
With us, you can experience rejuvenation, healing and relaxation through treatments ranging from Swedish Massage Therapy, Deep Tissue Massage, Sports Massage, Cupping Therapy and Hot Stones Therapy. Each therapy will soothe your muscles and mind, and are tailored to your individual needs. <br /><br />
Our massage treatments will help you achieve balance in your life, harness your full potential, and ultimately feel better. We teach you how to maintain harmony everyday for physical, mental and spiritual health and assist in maintaining your goals, whether short or long term. We realise that maintaining a healthy lifestyle is a daily challenge so escape today for <b>'Me'</b> time and restore your energy flow.<br /><br />
Please call or stop by for more information on services and current promotions.<br /><br />
<i>We hope to see you back again for new updates to our website. There's much more to come!</i><br />
<br />
<b><i>Call 0400 346 939 or <a href="/contact.php" style="text-decoration: underline;">click here</a> to book an appointment online.</i></b><br /><br />
MBS Escape are donating their services to assist the organisation of The Glitter Ball. This year, all proceeds from The Glitter Ball will be donated to the Advanced Plastic Surgery Education Research Foundation (APSEF). The funds raised will support research into treatments for scarring, including finding a cure for cystic fibrosis.<br />
<a style="text-decoration: underline;" href="http://www.glitterball.org/supporters">Glitter Ball Supporter</a>
</div>
<div class="clear">&nbsp;</div>
</div>