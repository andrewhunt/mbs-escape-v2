MBS Escape is based on the belief that our customers' needs are of the utmost importance. We are committed to meeting those needs.<br /><br />
<strong>Booking Conditions</strong><br />
Please note the following booking conditions in relation to Services:
<ul>
	<li>We recommend that bookings should be made at least 24 hrs prior to appointment to ensure availability</li>
	<li>Payment can be arranged in advance, or cash on the day.</li>
	<li>Availability is not guaranteed if appointments are re-scheduled on the day</li>
	<li>Cancellations on the day are non-refundable</li>
	<li>Gift Vouchers available to any amount</li>
</ul><br />
<strong>Initial Consultation</strong><br />
At your initial consultation you will be required to fill out a client intake form asking for information about your lifestyle, medical history, hobbies, occupation, eating and drinking habits which enables the therapist to determine what action or relevant techniques to apply for your treatment. Please arrive 10 minutes early for your appointment so as to allow time for filling out your client intake form.