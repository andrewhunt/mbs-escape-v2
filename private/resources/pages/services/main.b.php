<?php
foreach ($services as $service) {
	echo '<div>';
	 	echo '<div class="service_right">';
	 		echo '<strong>'.$service->name().'</strong>';
	 	echo '</div>';
	 	echo '<div class="clear"></div>';
	echo '</div>';
		echo '<div>';
		 	echo '<div class="service_left">';
		 		foreach ($service->getPrices() as $price) {
		 			echo $price->name() . ' - ';
		 			if($price->cost() == 0)
		 			{
			 			echo 'free';
		 			}
		 			else
		 			{
			 			printf("$%d", $price->cost());
		 			}
		 			
		 			echo '<br />';
		 		}
		 	echo '</div>';
		 	echo '<div class="service_right">';
		 		echo $service->description();
		 	echo '</div>';
			echo '<div class="clear">';
			echo '</div>';
		echo '</div>';
		echo '<br />';
}
?>